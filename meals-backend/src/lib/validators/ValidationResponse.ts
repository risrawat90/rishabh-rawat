class ValidationResponse<T = any> {
    public message: string;
    public success: boolean;
    public sanitizedData?: T;

    constructor(success: boolean, message: string, sanitizedData?: T){
        this.success = success;
        this.message = message;
        this.sanitizedData = sanitizedData;
    }
    public static success<T = any>(data?: T): ValidationResponse<T> {
        return new ValidationResponse<T>(true, '', data);
    }

    public static error<T = any>(message: string): ValidationResponse<T> {
        return new ValidationResponse<T>(false, message);
    }
}

export default ValidationResponse;
