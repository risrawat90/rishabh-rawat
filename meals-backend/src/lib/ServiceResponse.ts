class ServiceResponse<T = any> {
    public success: boolean;
    public httpCode?: number;
    public message: string;
    public data?: T;

    constructor(success: boolean, message: string, data?: T, httpCode?: number ) {
        this.success = success;
        this.message = message;
        this.data = data;
        this.httpCode = httpCode ? httpCode : (success ? 200 : 400);
    }

    public static success<T = any>(data: T, message?: string, httpCode?: number) {
        return new ServiceResponse<T>(true, message || '', data, httpCode );
    }

    public static error<T = any>(message: string, data?: T, httpCode?: number) {
        return new ServiceResponse<T>(false, message, data, httpCode);
    }

    public static notFoundError() {
        return new ServiceResponse<{}>(false, 'Resource not found', null, 404);
    }

    public static forbiddenAccess() {
        return new ServiceResponse<{}>(false, 'You are forbidden for this action', null, 403);
    }
}

export default ServiceResponse;
