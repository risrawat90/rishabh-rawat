import {createParamDecorator} from '@nestjs/common';
import IAuthDetail from '../db/interface/IAuthDetail';

const AuthDetail = createParamDecorator((data, req): IAuthDetail => {
  const user = req.user;
  const jwt = req.jwt;
  return {currentUser: user, jwtToken: jwt};

});

export  {
  AuthDetail
};
