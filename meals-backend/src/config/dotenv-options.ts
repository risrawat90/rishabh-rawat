import * as path from 'path';

const env  = process.env.NODE_ENV || 'development';
const p = path.join(process.cwd(), `env/.env.${env}`);

// tslint:disable-next-line:no-console
console.log(`Loading env from ${p}`);

const dotEnvOptions = {
    path: p,
};

export {dotEnvOptions};
