import * as dotenv from 'dotenv';
import {dotEnvOptions} from './dotenv-options';
// Make sure dbConfig is exported only after dotenv.config
dotenv.config(dotEnvOptions);

module.exports = {
    type: process.env.TYPEORM_CONNECTION,
    host: process.env.TYPEORM_HOST,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    port: Number.parseInt(process.env.TYPEORM_PORT, 10),
    logging: process.env.TYPEORM_LOGGING === 'true',
    entities: [process.env.TYPEORM_ENTITIES],
    migrationsRun: process.env.TYPEORM_MIGRATIONS_RUN === 'true',
    synchronize: process.env.TYPEORM_SYNCHRONIZE === 'true',
    migrations: [process.env.TYPEORM_MIGRATIONS],
    seeds: [`src/db/seeds/*.seed.ts`],
    cli: {
        migrationsDir: 'src/db/migrations',
        entitiesDir: 'src/db/models',
    },
};
