import {Injectable} from '@nestjs/common';
import RepoService from '../repo/repo.service';
import {Between, getConnection, Like, MoreThanOrEqual, Repository} from 'typeorm';
import UserEntity from '../../db/models/user.entity';
import MealEntity from '../../db/models/meal.entity';
import ServiceResponse from '../../lib/ServiceResponse';
import IAuthDetail from '../../db/interface/IAuthDetail';
import EnumRole from '../../db/enums/EnumRole';
import Can, {CanEnum} from '../../util/Can';
import EnumTable from '../../db/enums/EnumTable';

@Injectable()
class MealService {

    private mealRepo: Repository<MealEntity>;
    private userRepo: Repository<UserEntity>;

    constructor(private readonly repoService: RepoService) {
        this.userRepo = repoService.userRepo;
        this.mealRepo = repoService.mealRepo;
    }

    async getMeals({
                       title = '', dateMin = '0000-01-01', dateMax = '3000-12-31', timeMin = '00:00', timeMax = '23:59',
                       calorieMin = 0, calorieMax = 1000000, limit = 50, offset = 0
                   },
                   authDetail: IAuthDetail): Promise<ServiceResponse> {
        let limitedAccess = false;
        if (authDetail.currentUser.role === EnumRole.MANAGER || authDetail.currentUser.role === EnumRole.REGULAR)
            limitedAccess = true;

        const mealEntity: MealEntity[] = await this.mealRepo.find({
            where: [
                {
                    title: Like(`%${title}%`),
                    date: Between(dateMin, dateMax),
                    time: Between(timeMin, timeMax),
                    calories: Between(calorieMin, calorieMax),
                    userId: limitedAccess ? authDetail.currentUser.id : MoreThanOrEqual(0)
                },

            ],
            skip: offset,
            take: limit,
            order: {
                id: 'DESC'
            }
        });
        await this.addDailyCaloriesToMeals(mealEntity);
        return ServiceResponse.success(mealEntity.map(m => m.toJSON()));
    }

    async getMeal(id = 0, authDetail: IAuthDetail): Promise<ServiceResponse> {
        const meal: MealEntity = await this.mealRepo.findOne(id);
        if (meal) {
            if (Can.canUserToMeal(authDetail.currentUser, meal, CanEnum.CAN_VIEW_MEAL)) {
                await this.addDailyCaloriesToMeals([meal]);
                return ServiceResponse.success(meal.toJSON());
            } else return ServiceResponse.forbiddenAccess();
        } else return ServiceResponse.notFoundError();
    }

    async updateMeal(id = 0, {title = '', calories = 0, date = '', time = '', userId = 0}, authDetail: IAuthDetail): Promise<ServiceResponse> {
        const meal: MealEntity = await this.mealRepo.findOne(id);
        if (meal) {
            if (Can.canUserToMeal(authDetail.currentUser, meal, CanEnum.CAN_EDIT_MEAL)) {
                title = title || meal.title;
                calories = calories || meal.calories;
                date = date || meal.date;
                time = time || meal.time;
                if (authDetail.currentUser.role === EnumRole.ADMIN) {
                    userId = userId || meal.userId;
                } else userId = meal.userId;
                Object.assign(meal, {title, calories, date, time, userId});
                const error: string = await meal.validate();
                if (error)
                    return ServiceResponse.error(error);
                else {
                    await this.mealRepo.save(meal);
                    return ServiceResponse.success(meal.toJSON(), 'Meal Updated Successfully');
                }

            } else return ServiceResponse.forbiddenAccess();
        } else return ServiceResponse.notFoundError();
    }

    async createMeal({title = '', calories = 0, date = '', time = '', userId = 0}, authDetail: IAuthDetail): Promise<ServiceResponse> {
        if (Can.canUserToMeal(authDetail.currentUser, null, CanEnum.CAN_EDIT_MEAL)) {
            const meal = new MealEntity({title, calories, time, date, userId: authDetail.currentUser.id});
            if (authDetail.currentUser.role === EnumRole.ADMIN) {
                meal.userId = userId > 0 ? userId : authDetail.currentUser.id;
            }
            const error: string = await meal.validate();
            if (error) return ServiceResponse.error(error);
            else {
                await this.mealRepo.save(meal);
                return ServiceResponse.success(meal.toJSON(), 'Meal Created Successfully');
            }

        } else return ServiceResponse.forbiddenAccess();
    }

    async deleteMeal(id = 0, authDetail: IAuthDetail): Promise<ServiceResponse> {
        const meal: MealEntity = await this.mealRepo.findOne(id);
        if (meal) {
            if (Can.canUserToMeal(authDetail.currentUser, meal, CanEnum.CAN_EDIT_MEAL)) {
                await this.mealRepo.delete(meal.id);
                return ServiceResponse.success(null, 'Meal Deleted Successfully');
            } else return ServiceResponse.forbiddenAccess();
        } else return ServiceResponse.notFoundError();

    }

    private async addDailyCaloriesToMeals(meals: MealEntity[]) {
        const res = await getConnection()
            .createQueryBuilder()
            .select(`SUM(calories) as dailycalorie, date as date`)
            .from(MealEntity, EnumTable.MEALS)
            .groupBy('date')
            .getRawMany();

        const data = res.reduce((d, val) => {
            d[val.date] = val.dailycalorie;
            return d;
        }, {});

        for (const meal of meals) {
            meal.dailyCalorieIntake = data[meal.date];
            meal.dailyCalorieGoal = (await this.userRepo.findOne(meal.userId)).calorieGoal;
        }
        return meals;

    }

}

export default MealService;
