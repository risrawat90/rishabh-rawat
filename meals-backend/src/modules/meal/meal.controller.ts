import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpException,
    Param,
    Post,
    Put,
    Query,
    UseGuards
} from '@nestjs/common';
import MealService from './meal.service';
import ServiceResponse from '../../lib/ServiceResponse';
import IAuthDetail from '../../db/interface/IAuthDetail';
import {AuthDetail} from '../../decorators/auth-detail';
import AuthenticationGuard from '../../guards/authentication.guard';

@Controller('meal')
class MealController {

    constructor(private readonly mealService: MealService) {}

    @Post('/')
    @HttpCode(201)
    @UseGuards(AuthenticationGuard)
    async createMeal(@Body() body, @AuthDetail() authDetail: IAuthDetail) {
        if (body.hasOwnProperty('calories'))
            body.calories = Number.parseInt(body.calories, 10) || 0;
        const res: ServiceResponse = await this.mealService.createMeal(body, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Get(':id')
    @UseGuards(AuthenticationGuard)
    async getMeal( @Param('id') id: number, @AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.mealService.getMeal(id, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Put(':id')
    @UseGuards(AuthenticationGuard)
    async updateMeal( @Param('id') id: number, @Body() body,  @AuthDetail() authDetail: IAuthDetail) {
        if (body.hasOwnProperty('calories'))
            body.calories = Number.parseInt(body.calories, 10) || 0;
        const res: ServiceResponse = await this.mealService.updateMeal(id, body, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Delete(':id')
    @UseGuards(AuthenticationGuard)
    async delete( @Param('id') id: number, @AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.mealService.deleteMeal(id, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Get('/')
    @UseGuards(AuthenticationGuard)
    async getMeals(@Query() queries, @AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.mealService.getMeals(queries, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }
}

export default MealController;
