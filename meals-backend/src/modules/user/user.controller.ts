import {Body, Controller, Delete, Get, HttpException, Param, Post, Put, Query, UseGuards} from '@nestjs/common';
import UserService from './user.service';
import ServiceResponse from '../../lib/ServiceResponse';
import GenderEnum from '../../db/enums/GenderEnum';
import IAuthDetail from '../../db/interface/IAuthDetail';
import {AuthDetail} from '../../decorators/auth-detail';
import AuthenticationGuard from '../../guards/authentication.guard';

@Controller('user')
class UserController {

    constructor(private readonly userService: UserService) {}

    @Post('/login')
    async login(@Body() {email= '', password= ''}) {
        const res: ServiceResponse = await this.userService.login({email, password});
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Post('/register')
    async register(@Body() {email= '', password= '', name = '', sex = GenderEnum.MALE, calorieGoal= 0 }) {
        calorieGoal = Number.parseInt(calorieGoal + '', 10) || 0;
        const res: ServiceResponse = await this.userService.register({email, password, name, sex, calorieGoal});
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Get('/logout')
    @UseGuards(AuthenticationGuard)
    async logout(@AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.userService.logout({}, authDetail);
        return {data: res.data, message: res.message};
    }

    @Get('/details')
    @UseGuards(AuthenticationGuard)
    async getLoggedUser(@AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.userService.getUser(authDetail.currentUser.id, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Get(':id')
    @UseGuards(AuthenticationGuard)
    async getUser(@Param('id') id = 0, @AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.userService.getUser(id, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Put(':id')
    @UseGuards(AuthenticationGuard)
    async updateUser(@Param('id') id = 0, @Body() body, @AuthDetail() authDetail: IAuthDetail) {
        if (body.hasOwnProperty('calorieGoal'))
            body.calorieGoal = Number.parseInt(body.calorieGoal, 10) || 0;
        const res: ServiceResponse = await this.userService.updateUser(id, body, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Delete(':id')
    @UseGuards(AuthenticationGuard)
    async deleteUser(@Param('id') id = 0, @AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.userService.deleteUser(id, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Get('')
    @UseGuards(AuthenticationGuard)
    async getUsers(@Query() query, @AuthDetail() authDetail: IAuthDetail) {
        const res: ServiceResponse = await this.userService.getUsers(query, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

    @Post('')
    @UseGuards(AuthenticationGuard)
    async createUser(@Body() body, @AuthDetail() authDetail: IAuthDetail) {
        if (body.hasOwnProperty('calorieGoal'))
            body.calorieGoal = Number.parseInt(body.calorieGoal, 10) || 0;

        const res: ServiceResponse = await this.userService.createUser(body, authDetail);
        if (res.success)
            return {data: res.data, message: res.message};
        else throw new HttpException(res.message, res.httpCode);
    }

}

export default UserController;
