import {Injectable} from '@nestjs/common';
import RepoService from '../repo/repo.service';
import ServiceResponse from '../../lib/ServiceResponse';
import {Repository} from 'typeorm';
import UserEntity from '../../db/models/user.entity';
import Bcryptjs from 'bcryptjs';
import AuthenticationUtil from '../../util/AuthenticationUtil';
import GenderEnum from '../../db/enums/GenderEnum';
import EnumRole from '../../db/enums/EnumRole';
import IAuthDetail from '../../db/interface/IAuthDetail';
import Can, {CanEnum} from '../../util/Can';

@Injectable()
class UserService {

    private userRepo: Repository<UserEntity>;

    constructor(private readonly repoService: RepoService) {
        this.userRepo = repoService.userRepo;
    }

    async login({email = '', password = ''}): Promise<ServiceResponse> {
        const userEntity: UserEntity = await this.userRepo.findOne({where: {email: email.toLowerCase()}});
        if (userEntity) {
            if (Bcryptjs.compareSync(password, userEntity.passwordHash)) {
                const jwt = AuthenticationUtil.generateJWTToken(userEntity);
                await AuthenticationUtil.addJWTToken({jwtToken: jwt, user: userEntity});
                return ServiceResponse.success({...userEntity.toJSON(), jwt});
            } else return ServiceResponse.error('Invalid Credentials');

        } else {
            return ServiceResponse.error('Invalid Credentials');
        }
    }

    async register({
                       email = '', password = '', name = '', calorieGoal = 0, role = EnumRole.ADMIN, sex = GenderEnum.MALE
                   }): Promise<ServiceResponse> {
        if (await this.userRepo.findOne({where: {email: email.toLowerCase()}})) {
            return ServiceResponse.error('Email Already Registered');
        } else {
            const passwordHash = Bcryptjs.hashSync(password, 8);
            const user: UserEntity = new UserEntity({
                name,
                passwordHash,
                email: email.toLowerCase(),
                role,
                sex,
                calorieGoal
            });
            const errors: string = await user.validate();
            if (errors) {
                return ServiceResponse.error(errors);
            } else if (password.trim().length < 3)
                return ServiceResponse.error('Password should be minimum 3 characters long');
            else {
                await this.userRepo.save(user);
                return ServiceResponse.success(user.toJSON(), 'Register Successful', 201);
            }
        }
    }

    async logout({}, authDetail: IAuthDetail) {
        await AuthenticationUtil.removeJWTToken({jwtToken: authDetail.jwtToken, user: authDetail.currentUser});
        return ServiceResponse.success(null, 'Logged out successful');
    }

    async getUser(id: number, authDetail: IAuthDetail): Promise<ServiceResponse> {
        const user: UserEntity = await this.userRepo.findOne(id);
        if (user) {
            if (Can.canUserToUser(authDetail.currentUser, user, CanEnum.CAN_VIEW_USER)) {
                return ServiceResponse.success(user.toJSON());
            } else return ServiceResponse.forbiddenAccess();
        } else return ServiceResponse.notFoundError();
    }

    async updateUser(id: number, {name, role, email, sex, calorieGoal, password = ''}, authDetail: IAuthDetail): Promise<ServiceResponse> {
        const user: UserEntity = await this.userRepo.findOne(id);
        if (user) {
            if (Can.canUserToUser(authDetail.currentUser, user, CanEnum.CAN_EDIT_USER)) {
                user.name = name || user.name;
                user.email = email || user.email;
                user.sex = sex || user.sex;
                user.calorieGoal = calorieGoal || user.calorieGoal;
                user.role = authDetail.currentUser.role === EnumRole.ADMIN && role ? role : user.role;
                if (password) {
                    if (password.trim().length < 3) return ServiceResponse.error('Password can not be less than 3');
                    user.passwordHash = Bcryptjs.hashSync(password, 8);
                }
                const err: string = await user.validate();
                if (err) return ServiceResponse.error(err);
                await this.userRepo.save(user);
                return ServiceResponse.success(user.toJSON(), 'User updated successfully');
            } else return ServiceResponse.forbiddenAccess();
        } else return ServiceResponse.notFoundError();
    }

    async deleteUser(id: number, authDetail: IAuthDetail): Promise<ServiceResponse> {
        const user: UserEntity = await this.userRepo.findOne(id);
        if (user) {
            if (Can.canUserToUser(authDetail.currentUser, user, CanEnum.CAN_EDIT_USER)) {
                await this.userRepo.delete(user.id);
                return ServiceResponse.success(null, 'Resource deleted successfully');
            } else return ServiceResponse.forbiddenAccess();
        } else return ServiceResponse.notFoundError();
    }

    async getUsers({limit = 50, offset = 0}, authDetail: IAuthDetail): Promise<ServiceResponse> {
        if (Can.canUserToUser(authDetail.currentUser, null, CanEnum.CAN_VIEW_USERS)) {
            const users: UserEntity[] = await this.userRepo.find({
                skip: offset,
                take: limit,
                order: {
                    id: 'DESC'
                },
            });
            return ServiceResponse.success(users.map(m => m.toJSON()));
        } else return ServiceResponse.forbiddenAccess();
    }

    async createUser({
                         email = '', password = '', name = '', calorieGoal = 0,
                         role = EnumRole.REGULAR, sex = GenderEnum.MALE
                     },
                     authDetail: IAuthDetail) {
        if (authDetail.currentUser.role === EnumRole.REGULAR)
            return ServiceResponse.forbiddenAccess();
        if (authDetail.currentUser.role === EnumRole.MANAGER && [EnumRole.MANAGER, EnumRole.ADMIN].includes(role))
            return ServiceResponse.forbiddenAccess();

        const sr: ServiceResponse = await this.register({email, password, name, calorieGoal, role, sex});
        if (sr.success) {
            sr.message = 'User created successfully';
            return sr;
        } else return sr;
    }
}

export default UserService;
