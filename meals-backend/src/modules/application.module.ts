import * as path from 'path';
import {Module} from '@nestjs/common';
import {dotEnvOptions} from '../config/dotenv-options';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ConfigModule, ConfigService} from 'nestjs-config';
import RepoModule from './repo/repo.module';
import UserModule from './user/user.module';
import MealModule from "./meal/meal.module";

@Module({
    imports: [
        ConfigModule.load(path.resolve(__dirname, '..', '**/*config!(*.d).{ts,js}'), {
            modifyConfigName: name => name.replace('.config', ''),
            ...dotEnvOptions,
        }),
        TypeOrmModule.forRootAsync({
            useFactory: (config: ConfigService) => config.get('database'),
            inject: [ConfigService],
        }),
        RepoModule,
        UserModule,
        MealModule
    ],
})
class ApplicationModule {

}

export default ApplicationModule;
