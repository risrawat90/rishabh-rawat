import {Repository} from 'typeorm';
import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import UserEntity from '../../db/models/user.entity';
import MealEntity from '../../db/models/meal.entity';

@Injectable({
})
class RepoService {

    public constructor(
        @InjectRepository(UserEntity) public userRepo: Repository<UserEntity>,
        @InjectRepository(MealEntity) public mealRepo: Repository<MealEntity>,
    ) {

    }

    public getRepos() {
        return {
            userRepo: this.userRepo,
            mealRepo: this.mealRepo,
        };
    }
}

export default RepoService;
