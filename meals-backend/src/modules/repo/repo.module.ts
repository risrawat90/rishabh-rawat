import {Global, Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import RepoService from './repo.service';
import UserEntity from '../../db/models/user.entity';
import MealEntity from '../../db/models/meal.entity';

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([
            UserEntity,
            MealEntity
        ]),
    ],
    providers: [RepoService],
    exports: [RepoService],
})
class RepoModule {

}

export default RepoModule;
