import {CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';
import {validate as Validator, ValidationError, ValidatorOptions} from 'class-validator';

export declare type Diff<T extends string | symbol | number, U extends string | symbol | number> = ({
    [P in T]: P;
} & {
    [P in U]: never;
} & {
    [x: string]: never;
})[T];
export declare type Omit<T, K extends keyof T> = {
    [P in Diff<keyof T, K>]: T[P];
};
export declare type RecursivePartial<T> = {
    [P in keyof T]?: RecursivePartial<T[P]>;
};
export declare type NonAbstract<T> = {
    [P in keyof T]: T[P];
};

export type FilteredModelAttributes<T extends ModelEntity<T>> =
    RecursivePartial<Omit<T, keyof ModelEntity<any>>> & {
    id?: number | any;
    createdAt?: Date | any;
    updatedAt?: Date | any;
    deletedAt?: Date | any;
    version?: number | any;
};

class ModelEntity<T extends ModelEntity<T>> {
    constructor(values?: FilteredModelAttributes<T>) {
        Object.assign<{}, FilteredModelAttributes<T>>(this, values);
    }

    @PrimaryGeneratedColumn()
    public id: number;

    @CreateDateColumn({name: 'created_at'})
    public createdAt: Date;

    @UpdateDateColumn({name: 'updated_at'})
    public updatedAt: Date;

    async validate(option?: ValidatorOptions): Promise<string | null> {
        const err: ValidationError[] = await Validator(this, option || {});
        if (err.length > 0) {
            return Object.values(err[0].constraints).join(' | ');
        } else { return null; }
    }

    async isValid(option?: ValidatorOptions): Promise<boolean> {
        const error: string = await this.validate(option);
        return !!error;
    }

    public toJSON(includes = [], skips = []) {
        const d = {};
        for (const key of includes) {
            d[key] = this[key];
        }

        for (const key of skips) {
            delete d[key];
        }

        return d;
    }
}

export default ModelEntity;
