import {Column, Entity, OneToMany} from 'typeorm';
import ModelEntity from './ModelEntity';
import EnumTable from '../enums/EnumTable';
import EnumRole from '../enums/EnumRole';
import {IsEmail, IsIn, IsInt, Length, Max, Min} from 'class-validator';
import GenderEnum from '../enums/GenderEnum';
import MealEntity from "./meal.entity";

@Entity({name: EnumTable.USERS})
class UserEntity extends ModelEntity<UserEntity> {

    @Length(3, 255, {message: 'Name Length should be between 3 & 255'})
    @Column()
    public name: string;

    @IsIn(Object.values(EnumRole))
    @Column({type: 'varchar'})
    public role: EnumRole;

    @Column({name: 'password_hash'})
    public passwordHash: string;

    @IsEmail({allow_display_name: true, allow_utf8_local_part: true, require_tld: false}) @Column({name: 'email'})
    public email: string;

    @IsIn(Object.values(GenderEnum))
    @Column({type: 'varchar'})
    public sex: GenderEnum;

    @Min(1) @Max(10000) @IsInt()
    @Column({name: 'calorie_goal', type: 'int'})
    public calorieGoal: number;

    @OneToMany(type => MealEntity, meal => meal.user)
    public meals: MealEntity[];

    public toJSON(includes = ['id', 'name', 'role', 'email', 'sex', 'calorieGoal'], skips = []): {} {
        const d: any = super.toJSON(includes, skips);
        return d;
    }

}

export default UserEntity;
