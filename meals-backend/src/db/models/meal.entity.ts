import {Column, Entity, JoinColumn, ManyToOne} from 'typeorm';
import ModelEntity from './ModelEntity';
import EnumTable from '../enums/EnumTable';
import {
    IsInt,
    Length,
    Max,
    Min,
    Validate,
    ValidationArguments,
    ValidatorConstraint,
    ValidatorConstraintInterface
} from 'class-validator';
import UserEntity from './user.entity';
import moment from 'moment';

@ValidatorConstraint()
class ValidDate implements ValidatorConstraintInterface {

    validate(text: string, args: ValidationArguments) {
        return moment(text, 'YYYY-MM-DD', true).isValid();
    }

    defaultMessage(args: ValidationArguments) { // here you can provide default error message if validation failed
        return 'Not an Valid Date, Date should be YYYY-MM-DD format only';
    }

}

@ValidatorConstraint()
class ValidTime implements ValidatorConstraintInterface {

    validate(text: string, args: ValidationArguments) {
        return moment(`2010-02-02 ${text}`, 'YYYY-MM-DD HH:mm', true).isValid();
    }

    defaultMessage(args: ValidationArguments) { // here you can provide default error message if validation failed
        return 'Not an Valid TIME, TIME should be HH:MM format only';
    }

}

@Entity({name: EnumTable.MEALS})
class MealEntity extends ModelEntity<MealEntity> {

    @Length(3, 255, {message: 'Title Length should be between 3 & 255'})
    @Column()
    public title: string;

    @Min(1) @Max(10000) @IsInt()
    @Column({type: 'int'})
    public calories: number;

    @Validate(ValidDate)
    @Column()
    public date: string;

    @Validate(ValidTime)
    @Column()
    public time: string;

    @Column({name: 'user_id'})
    public userId: number;

    @ManyToOne(type => UserEntity, user => user.meals)
    @JoinColumn({name: 'user_id'})
    public user: UserEntity;

    public dailyCalorieIntake: number;
    public dailyCalorieGoal: number;

    public toJSON(includes = ['id', 'title', 'calories', 'date', 'time', 'userId', 'dailyCalorieIntake', 'dailyCalorieGoal'], skips = []): {} {
        const d: any = super.toJSON(includes, skips);
        return d;
    }

}

export default MealEntity;
