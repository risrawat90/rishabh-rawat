enum EnumTable {
    MEALS = 'meals',
    USERS = 'users',
}

export default EnumTable;
