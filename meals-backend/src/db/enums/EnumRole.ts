enum EnumRole {
    ADMIN = 'admin',
    MANAGER = 'manager',
    REGULAR = 'regular',
}

export default EnumRole;
