import UserEntity from '../models/user.entity';

export default interface IAuthDetail {
    currentUser: UserEntity;
    jwtToken: string;
}
