import {MigrationInterface, QueryRunner, Table} from 'typeorm';
import EnumTable from '../enums/EnumTable';
import MigrationUtil from '../../util/MigrationUtil';

export class CreateMealTable1562228123084 implements MigrationInterface {

    private static readonly mealTable = new Table({
        name: EnumTable.MEALS,
        columns: [
            ...MigrationUtil.getIDAndDatesColumns(),
            {
                name: 'title',
                type: 'varchar',
                length: '255',
                isPrimary: false,
                isNullable: false,
            }, {
                name: 'calories',
                type: 'int',
            }, {
                name: 'user_id',
                type: 'int',
            }, {
                name: 'time',
                type: 'varchar',
                length: '255',
            }, {
                name: 'date',
                type: 'varchar',
                length: '255',
            }
        ],
    });

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(CreateMealTable1562228123084.mealTable);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable(CreateMealTable1562228123084.mealTable);

    }

}
