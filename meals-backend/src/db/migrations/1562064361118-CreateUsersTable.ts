import {MigrationInterface, QueryRunner, Table} from 'typeorm';
import EnumTable from '../enums/EnumTable';
import MigrationUtil from '../../util/MigrationUtil';

export class CreateUsersTable1562064361118 implements MigrationInterface {

  private static readonly usersTable = new Table({
    name: EnumTable.USERS,
    columns: [
      ...MigrationUtil.getIDAndDatesColumns(),
      {
        name: 'name',
        type: 'varchar',
        length: '255',
        isPrimary: false,
        isNullable: false,
      }, {
        name: 'email',
        type: 'varchar',
        length: '255',
        isUnique: true,
      }, {
        name: 'role',
        type: 'varchar',
        length: '255',
      }, {
        name: 'password_hash',
        type: 'varchar',
        length: '255',
      }, {
        name: 'sex',
        type: 'varchar',
        length: '255',
      }, {
        name: 'calorie_goal',
        type: 'int',
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(CreateUsersTable1562064361118.usersTable);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable(CreateUsersTable1562064361118.usersTable);
  }

}
