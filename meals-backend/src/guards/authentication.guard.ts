import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import RepoService from '../modules/repo/repo.service';
import {oc} from 'ts-optchain';
import AuthenticationUtil from '../util/AuthenticationUtil';
import UserEntity from '../db/models/user.entity';

@Injectable()
class AuthenticationGuard implements CanActivate {

    constructor(private readonly repoService: RepoService) {}

    async canActivate(context: ExecutionContext): | Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const jwtToken = oc<any>(request).headers.jwt('');
        const user: UserEntity = await AuthenticationUtil.getUserFromJWTToken(jwtToken, this.repoService);
        if (user) {
            request.user = user;
            return true;
        } else return false;
    }

}

export default AuthenticationGuard;
