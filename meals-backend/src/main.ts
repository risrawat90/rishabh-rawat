import ApplicationModule from './modules/application.module';
import {NestFactory} from '@nestjs/core';

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);
  app.enableCors();
  await app.listen(process.env.EXPRESS_PORT || 3000);
}
bootstrap();
