import {NestFactory} from '@nestjs/core';
import ApplicationModule from './modules/application.module';
import {INestApplicationContext} from '@nestjs/common';
import RepoService from './modules/repo/repo.service';
import RepoModule from './modules/repo/repo.module';
import UserEntity from './db/models/user.entity';
import Bcryptjs from 'bcryptjs';
import EnumRole from './db/enums/EnumRole';
import GenderEnum from './db/enums/GenderEnum';
import MealEntity from './db/models/meal.entity';
import Lodash from 'lodash';
import Faker from 'faker';
import moment from 'moment';

class Seed {

    static async run() {
        const ctx: INestApplicationContext = await NestFactory.createApplicationContext(ApplicationModule);

        const repoService: RepoService = ctx.select(RepoModule).get(RepoService);
        const hashed = Bcryptjs.hashSync('1234', 8);
        const u1 = new UserEntity({
            name: 'user Reg',
            email: 'user1.regular@gmail.com',
            passwordHash: hashed,
            role: EnumRole.REGULAR,
            sex: GenderEnum.MALE,
            calorieGoal: 1000
        });
        await repoService.userRepo.save(u1);
        const u2 = new UserEntity({
            name: 'user Mag',
            email: 'user1.manager@gmail.com',
            passwordHash: hashed,
            role: EnumRole.MANAGER,
            sex: GenderEnum.MALE,
            calorieGoal: 1000
        });
        await repoService.userRepo.save(u2);
        const u3 = new UserEntity({
            name: 'user Adm',
            email: 'user1.admin@gmail.com',
            passwordHash: hashed,
            role: EnumRole.ADMIN,
            sex: GenderEnum.MALE,
            calorieGoal: 1000
        });
        await repoService.userRepo.save(u3);

        for (let i = 0 ; i < 100 ; i++) {
            const meal = new MealEntity({
                user: Lodash.sample([u1, u2, u3]),
                title: Faker.name.findName(),
                date: moment(Faker.date.between('2019-07-03', '2019-07-05')).format('YYYY-MM-DD'),
                time: moment(Faker.date.between('2019-07-03', '2019-07-05')).format('HH:mm'),
                calories: Number.parseInt((Math.random() * 1000) % 500 + 100 + '', 10),
            });
            await repoService.mealRepo.save(meal);
        }
    }

}

Seed.run();
