import {TableColumnOptions} from 'typeorm/schema-builder/options/TableColumnOptions';
import {TableForeignKey} from 'typeorm';

class MigrationUtil {
    public static getIDAndDatesColumns(): TableColumnOptions[] {
        const columns: TableColumnOptions[] = [];
        columns.push({
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isNullable: false,
        });
        columns.push({
            name: 'created_at',
            type: 'timestamptz',
            isPrimary: false,
            isNullable: false,
            default: 'now()',
        });
        columns.push({
            name: 'updated_at',
            type: 'timestamptz',
            isPrimary: false,
            isNullable: false,
            default: 'now()',
        });
        return columns;
    }

    public static getFKColumns(columnNames: string[]): TableColumnOptions[] {

        const columns: TableColumnOptions[] = columnNames.map<TableColumnOptions>(columnName => {
            return {
                name: columnName,
                type: 'uuid',
                isPrimary: false,
                isNullable: false,
            };
        });
        return columns;
    }

    public static createForeignKey(relationTable: string, referenceTable: string, columnName: string,
                                   referenceColumnName?: string, name?: string ): TableForeignKey {
        name = name || `fk_${referenceTable}_${relationTable}`;
        referenceColumnName =  referenceColumnName || 'id';
        return new TableForeignKey({
            name,
            columnNames: [columnName],
            referencedColumnNames: [referenceColumnName],
            referencedTableName: referenceTable,
            onDelete: 'CASCADE',
        });
    }
}

export default MigrationUtil;
