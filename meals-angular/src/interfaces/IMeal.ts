export interface IMeal {
  id: number;
  date: string;
  time: string;
  title: string;
  calories: number;
  dailyCalorieIntake?: number;
  dailyCalorieGoal?: number;
}
