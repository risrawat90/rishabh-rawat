import {ERole} from '../enums/ERole';
import {EGender} from '../enums/EGender';

export interface IUser {
  id?: number;
  name: string;
  role: ERole;
  email: string;
  sex: EGender;
  calorieGoal: number;
  password?: string;
}

