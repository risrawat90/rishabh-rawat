export enum ERole {
  CONSUMER = 'regular',
  MANAGER = 'manager',
  ADMIN = 'admin'
}
