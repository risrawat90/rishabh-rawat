export class LoginAction {
  static readonly type = '[USER] login';
  constructor(public  payload) {}
}

export class LogoutAction {
  static readonly type = '[USER] logout';
}

export class LoginActionOnRefresh {
  static readonly type = '[USER] login_refresh';
  constructor(public payload) {}
}
