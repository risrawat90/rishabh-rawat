import {IUser} from '../../interfaces/IUser';

export interface LoginModel {
  user: IUser;
  isLoggedIn: boolean;
}
