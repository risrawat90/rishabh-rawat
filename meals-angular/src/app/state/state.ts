import {Action, Selector, State, StateContext} from '@ngxs/store';
import {LoginModel} from './login.model';
import {LoginAction, LoginActionOnRefresh, LogoutAction} from './state.actions';
import {IUser} from '../../interfaces/IUser';
import {ERole} from '../../enums/ERole';
import {EGender} from '../../enums/EGender';
//
const role: ERole = ERole.ADMIN;
const sex: EGender = EGender.MALE;
const userDefault: IUser = {
  id: 0,
  name: 'initName',
  email: 'initEmail',
  sex,
  role,
  calorieGoal: 1000,
};

@State<LoginModel>({
  name: 'login',
  defaults: {
    user: userDefault,
    isLoggedIn: false,
  }
})

export class LoginState {
  constructor() {
  }

  @Selector()
  static readLogin(state: LoginModel) {
    return state;
  }


  @Action(LoginAction)
  login({patchState, getState}: StateContext<LoginModel>, payload) {
    localStorage.setItem('jwttoken', payload.payload.jwt);
    patchState({user: payload.payload, isLoggedIn: true});
  }

  @Action(LogoutAction)
  logout({patchState}: StateContext<LoginModel>) {
    localStorage.removeItem('jwttoken');
    patchState({isLoggedIn: false, user: userDefault});
  }

  @Action(LoginActionOnRefresh)
  loginActionOnRefresh({patchState, getState}: StateContext<LoginModel>, payload) {
    console.log('refresh action');
    console.log(payload);
    patchState({isLoggedIn: true, user: payload.payload});
}

}
