import {Component, OnInit} from '@angular/core';
import {Select} from '@ngxs/store';
import {LoginState} from '../state/state';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {IUser} from '../../interfaces/IUser';
import {ERole} from '../../enums/ERole';

@Component({
  selector: 'app-navbar',
  template: `
    <mat-toolbar class="navbar" *ngIf="loggedIn">
      <div class="nav-links">
        <span (click)="navClickHandler(0)">Create Meal</span>
        <span (click)="navClickHandler(1)">View Meals</span>
        <span *ngIf="isAdminOrManager()" (click)="navClickHandler(4)">Create User</span>
        <span *ngIf="isAdminOrManager()" (click)="navClickHandler(3)">View Users</span>
      </div>
      <div class="logout">
        <span (click)="navClickHandler(2)">Home</span>
        <span (click)="loginClickHandler(1)">Logout</span>
      </div>
    </mat-toolbar>
    <mat-toolbar color="accent" class="navbar" *ngIf="!loggedIn">
      <div class="logo"><span>Meals</span></div>
      <div class="logout">
        <span class="register" (click)="loginClickHandler(2)">Register</span>
        <span (click)="loginClickHandler(0)">Login</span>
      </div>
    </mat-toolbar>
  `,
  styles: [`
    .navbar {
      position: relative;
      background-color: #f2a2e4;
    }

    .logo {
      padding-right: 4rem;
    }

    .logo span {
      color: white;
    }

    .nav-links span {
      padding-right: 20px;
      text-align: center;
      display: inline-block;
      cursor: pointer;
      color: white;
    }

    .logout {
      position: absolute;
      right: 1rem;
    }

    .logout span {
      padding-right: 20px;
      cursor: pointer;
      color: white;
    }

    .register {
      padding-right: 3rem;
    }
  `]
})
export class NavbarContainer implements OnInit {
  @Select(LoginState) data$: Observable<LoginModel>;

  loggedIn = false;
  user: IUser = null;

  constructor(private toastr: ToastrService, private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.data$.subscribe(u => {
      this.loggedIn = u.isLoggedIn;
      this.user = u.user;
    });
  }

  isAdminOrManager() {
    return this.user && (this.user.role === ERole.ADMIN || this.user.role === ERole.MANAGER);
  }

  navClickHandler(index) {
    switch (index) {
      case 0 :
        this.router.navigateByUrl('meal-create');
        break;
      case 1 :
        this.router.navigateByUrl('meal-list');
        break;
      case 2 :
        this.router.navigateByUrl('home');
        break;
      case 3 :
        this.router.navigateByUrl('user-list');
        break;
      case 4 :
        this.router.navigateByUrl('user-create');
        break;
    }
  }

  async loginClickHandler(index) {
    // 0 => Currently user is not signed in and wants to sign in.
    // 1 => User Is Signed in and wants to sign out.
    if (index === 0) {
      this.router.navigateByUrl('login');
    } else if (index === 1) {
      // dispatch Logout Action Here
      this.userService.logout().then(() => {
        this.router.navigateByUrl('login');
      }).catch((e) => {
        this.toastr.error(e);
      });
    } else if (index === 2) {
      this.router.navigateByUrl('register');
    }
  }
}

