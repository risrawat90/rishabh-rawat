import {Component, OnDestroy, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {LoginState} from '../state/state';
import {Observable, Subscription} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {IUser} from '../../interfaces/IUser';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../services/user.service';
import {IResponse} from '../../interfaces/IResponse';
import {ERole} from '../../enums/ERole';
import {EGender} from '../../enums/EGender';

@Component({
  selector: 'app-home',
  template: `
    <mat-card class="user-card" (click)="navigateToEditProfile()">
      <img mat-card-image
           src="https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1"
           alt="User Icon Image">
      <mat-card-title>{{this.user.name}}</mat-card-title>
      <mat-card-subtitle>Role: {{this.user.role}}</mat-card-subtitle>
      <mat-card-subtitle>Gender: {{this.user.sex}}</mat-card-subtitle>
      <mat-card-subtitle>Email: {{this.user.email}}</mat-card-subtitle>
      <mat-card-subtitle>Id: {{this.user.id}}</mat-card-subtitle>
      <mat-card-subtitle>Calorie Goal: {{this.user.calorieGoal}}</mat-card-subtitle>
    </mat-card>
    <!--    <div class="user-create-form" *ngIf="canCreateUser()">-->
    <!--      <div class="user-create-heading">-->
    <!--        <span>User Creation </span>-->
    <!--      </div>-->
    <!--      <div class="user-create-input">-->
    <!--        <mat-form-field appearance="outline">-->
    <!--          <mat-label>Name</mat-label>-->
    <!--          <input [(ngModel)]="name" matInput placeholder="Aaron">-->
    <!--        </mat-form-field>-->
    <!--      </div>-->


    <!--      <div class="user-create-input">-->
    <!--        <mat-form-field appearance="outline">-->
    <!--          <mat-label>Email</mat-label>-->
    <!--          <input [(ngModel)]="email" matInput placeholder="frankcastle@gmail.com">-->
    <!--        </mat-form-field>-->
    <!--      </div>-->

    <!--      <div class="user-create-input">-->
    <!--        <mat-form-field appearance="outline">-->
    <!--          <mat-label>Password</mat-label>-->
    <!--          <input [(ngModel)]="password" matInput type="password" placeholder="*****">-->
    <!--        </mat-form-field>-->
    <!--      </div>-->

    <!--      <div class="user-create-input">-->
    <!--        <mat-form-field appearance="outline">-->
    <!--          <mat-label>Calorie Goal</mat-label>-->
    <!--          <input [(ngModel)]="calorieGoal" matInput placeholder="2000">-->
    <!--        </mat-form-field>-->
    <!--      </div>-->

    <!--      <div class="user-create-input">-->
    <!--        <mat-form-field appearance="outline">-->
    <!--          <mat-label>Role</mat-label>-->
    <!--          <mat-select [(ngModel)]="role">-->
    <!--            <mat-option *ngFor="let roleVal of roleArray" [value]="roleVal">-->
    <!--              {{roleVal}}-->
    <!--            </mat-option>-->
    <!--          </mat-select>-->
    <!--        </mat-form-field>-->
    <!--      </div>-->

    <!--      <div class="user-create-input">-->
    <!--        <mat-form-field appearance="outline">-->
    <!--          <mat-label>Gender</mat-label>-->
    <!--          <mat-select [(ngModel)]="gender">-->
    <!--            <mat-option *ngFor="let genderVal of genderArray" [value]="genderVal" >-->
    <!--              {{genderVal}}-->
    <!--            </mat-option>-->
    <!--          </mat-select>-->
    <!--        </mat-form-field>-->
    <!--        <div *ngIf="!update">-->
    <!--          <button (click)="createUser()" mat-raised-button color="primary">Create User</button>-->
    <!--        </div>-->
    <!--        <div *ngIf="update">-->
    <!--          <button (click)="updateUser()" mat-raised-button color="primary">Update User</button>-->
    <!--          <button class="deleteUserBtn" (click)="deleteUser()" mat-raised-button color="primary">Delete User</button>-->
    <!--        </div>-->

    <!--      </div>-->
    <!--    </div>-->
  `,
  styles: [`
    .user-card {
      max-width: 400px;
    }

    .user-card img {
      max-width: 8rem;
    }

    .user-card {
      text-align: center;
      margin: 0 auto;
    }

    .user-create-form {
      padding-top: 3rem;
      width: 50%;
      text-align: center;
      margin: 0 auto;
    }

    .user-create-heading span {
      font-size: 3rem;
      font-family: "BryantProRegular", 'Helvetica Neue', helvetica, arial, sans-serif, "Helvetica Neue";
    }

    .user-create-input mat-form-field {
      width: 70%;
    }

    .deleteUserBtn {
      margin: 1rem;
    }
  `]
})
export class HomeContainer implements OnInit, OnDestroy {
  subscription: Subscription;
  @Select(LoginState) stateData$: Observable<LoginModel>;
  user: IUser;
  name = '';
  role: ERole;
  email = '';
  gender: EGender;
  password = '';
  id;
  update = false;
  calorieGoal;

  roleArray = Object.values(ERole);
  genderArray = ['male', 'female', 'other'];

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private store: Store,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    this.subscription = this.stateData$.subscribe(u => {
      this.user = u.user;
    });


    if (this.id) {
      const id = this.id;
      this.update = true;
      console.log(id);
      this.userService.getUserById(id).then((res: IResponse) => {
        console.log(res);
        const user = res.data;
        this.name = user.name;
        this.role = user.role;
        this.email = user.email;
        this.gender = user.sex;
        this.calorieGoal = user.calorieGoal;
      });
    }

  }

  navigateToEditProfile() {
    this.router.navigateByUrl(`user-update/${this.user.id}`);
  }

  // canCreateUser() {
  //   const x: boolean = this.role && (this.role === 'admin' || this.role === 'manager');
  //   debugger;
  //   return x;
  // }

  // async createUser() {
  //   const userObj = {
  //     name: this.name,
  //     password: this.password,
  //     role: this.role,
  //     email: this.email,
  //     sex: this.gender,
  //     calorieGoal: parseInt(this.calorieGoal, 10)
  //   };
  //
  //   let response: {success: boolean, message?: string};
  //   response = ValidationService.isValidCreateUser({...userObj, password: this.password});
  //   console.log(response);
  //   if (!response.success) {
  //     this.toastr.error(response.message);
  //   } else {
  //     this.userService.createUser(userObj).then((res) => {
  //       this.toastr.success('User Created Successfully.');
  //       this.router.navigateByUrl('user-list');
  //     }).catch((e) => {
  //       this.toastr.error(e);
  //     });
  //   }
  //
  // }

  // async updateUser() {
  //   const userObj: IUser = {
  //     id: this.id,
  //     name: this.name,
  //     role: this.role,
  //     email: this.email,
  //     sex: this.gender,
  //     calorieGoal: parseInt(this.calorieGoal, 10)
  //   };
  //   let response: { success: boolean, message?: string };
  //   response = ValidationService.isValidCreateUser({...userObj, password: this.password});
  //   console.log(response);
  //   if (!response.success) {
  //     this.toastr.error(response.message);
  //   } else {
  //     this.userService.updateUserById(userObj).then(() => {
  //       this.toastr.success('User Updated Successfully');
  //       this.router.navigateByUrl('user-list');
  //     }).catch((e) => {
  //       this.toastr.error(e);
  //     });
  //   }
  // }

  // async deleteUser() {
  //   this.userService.deleteUserById(this.id).then((res) => {
  //     this.toastr.success('Record Deleted Successfully');
  //     this.router.navigateByUrl('user-list');
  //   }).catch((e) => {
  //     this.toastr.error(e);
  //   }).catch((e) => {
  //     this.toastr.error(e);
  //   });
  // }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

  }
}
