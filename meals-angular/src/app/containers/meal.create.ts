import {Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {IMeal} from '../../interfaces/IMeal';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginState} from '../state/state';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {ToastrService} from 'ngx-toastr';
import {MealService} from '../services/meal.service';
import ValidationService from '../services/validation.service';
import {IResponse} from "../../interfaces/IResponse";

@Component({
  selector: 'app-meal-create',
  template: `
    <div class="meal-create-container">
      <div class="meal-create-form">
      <div class="meal-create-input">
      <mat-form-field appearance="outline">
        <mat-label>Meal Name</mat-label>
        <input [(ngModel)]="mealName" matInput placeholder="Pasta, burger">
      </mat-form-field>
      </div>
      <div class="meal-create-input">
      <mat-form-field appearance="outline">
        <mat-label>Calorie</mat-label>
        <input [(ngModel)]="calorie" type="number" matInput placeholder="Calorie">
      </mat-form-field>
      </div>
      <div class="meal-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Date</mat-label>
          <input [(ngModel)]="date" type="string" matInput placeholder="2019-23-11">
        </mat-form-field>
      </div>
        <div class="meal-create-input">
          <mat-form-field appearance="outline">
            <mat-label>Time</mat-label>
            <input [(ngModel)]="time" type="string" matInput placeholder="15:30">
          </mat-form-field>
      </div>

      <button *ngIf="!update" (click)="createMeal()" mat-raised-button color="primary">Create Meal</button>
      <button *ngIf="update" (click)="updateMeal()" mat-raised-button color="primary">Update Meal</button>
      <button class="deleteButton" *ngIf="update" (click)="deleteMeal()" mat-raised-button color="primary">Delete Meal</button>

      </div>
    </div>
  `,
  styles: [`
  .meal-create-form{
    width:60%;
    margin:0 auto;
    text-align: center;
  }
    .meal-create-input mat-form-field{
      width: 50%;
    }
    .deleteButton{
      margin: 1rem;
    }
  `]
})
export class MealCreateContainer implements OnInit {
    mealName = '';
    calorie = '';
    update = false;
    id;
    x;
    date;
    time;
  @Select(LoginState) data$: Observable<LoginModel>;

  constructor(private mealService: MealService,
              private store: Store,
              private route: ActivatedRoute,
              private router: Router,
              private toastr: ToastrService) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      const id = this.id;
      this.update = true;
      this.mealService.getMealById(id).then((res: IResponse) => {
        const item = res.data;
        this.mealName = item.title;
        this.calorie = item.calories.toString();
        this.date = item.date;
        this.time =  item.time;
      }).catch((e) => {
        this.toastr.error(e);
      });
       } else {
      this.update = false;
    }
  }
  async createMeal() {
  const mealObject: IMeal = {
    title: this.mealName,
    calories: parseInt(this.calorie, 10),
    id: 0,
    date: this.date,
    time: this.time
  };
  let response: {success: boolean, title?: string, message?: string};
  response = ValidationService.isValidMeal(mealObject);
  if (!response.success) {
      this.toastr.error(response.message, response.title);
    } else {
    this.mealService.createMeal(mealObject).then(() => {
      this.router.navigateByUrl('meal-list');
      this.toastr.success('Meal Added Successfully');
    }).catch((e) => {
      this.toastr.error(e);
    });
    }

  }

  async updateMeal() {
    const mealObject: IMeal = {
      title: this.mealName,
      calories: parseInt(this.calorie, 10),
      id: this.id,
      date: this.date,
      time: this.time
    };
    let response: {success: boolean, title?: string, message?: string};
    response = ValidationService.isValidMeal(mealObject);
    if (!response.success) {
      this.toastr.error(response.message, response.title);
    } else {
      this.mealService.updateMealById(mealObject).then(() => {
      this.toastr.success('Meal Updated Successfully');
      this.router.navigateByUrl('meal-list');
    }).catch((e) => {
        this.toastr.error(e);
      });
  }
  }

  deleteMeal() {
    this.mealService.deleteMealById(this.id).then((res) => {
      this.toastr.success('Meal Deleted Successfully');
      this.router.navigateByUrl('meal-list');
    }).catch((e) => {
      this.toastr.error(e);
    });
  }

}
