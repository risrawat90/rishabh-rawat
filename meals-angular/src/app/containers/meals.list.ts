import {Component, OnInit} from '@angular/core';
import {Select} from '@ngxs/store';
import {LoginState} from '../state/state';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {IMeal} from '../../interfaces/IMeal';
import {Router} from '@angular/router';
import {MealService} from '../services/meal.service';
import {ToastrService} from 'ngx-toastr';
import {IResponse} from '../../interfaces/IResponse';
import ValidationService from '../services/validation.service';

@Component({
  selector: 'app-meals-list',
  template: `
    <div class="parentContainer">
    <div class="filterBox">
      <div class="filterBox-field">
      <mat-form-field appearance="outline">
        <mat-label>Min Calorie:</mat-label>
      <input matInput [(ngModel)]="calorieMin" >
      </mat-form-field>
      </div>
      <div class="filterBox-field">
        <mat-form-field appearance="outline">
        <mat-label>Max Calorie:</mat-label>
        <input matInput [(ngModel)]="calorieMax" >
      </mat-form-field>
      </div>
      <div class="filterBox-field">
        <mat-form-field appearance="outline">
          <mat-label>Min Time:</mat-label>
          <input matInput [(ngModel)]="timeMin" >
        </mat-form-field>
      </div>
      <div class="filterBox-field">
        <mat-form-field appearance="outline">
          <mat-label>Max Time:</mat-label>
          <input matInput [(ngModel)]="timeMax" >
        </mat-form-field>
      </div>
      <div class="filterBox-field">
        <mat-form-field appearance="outline">
          <mat-label>Min Date:</mat-label>
          <input matInput [(ngModel)]="dateMin" >
        </mat-form-field>
      </div>
      <div class="filterBox-field">
        <mat-form-field appearance="outline">
          <mat-label>Max Date:</mat-label>
          <input matInput [(ngModel)]="dateMax" >
        </mat-form-field>
      </div>
      <button mat-button type="submit" (click)="applyFilters()">Apply</button>
    </div>
<div class="mealCards-container">

    <mat-card (click)="updateMeal(card.id)" class="mealCard" *ngFor="let card of tempMealArray">

      <img mat-card-image src="https://www.qsrmagazine.com/sites/default/files/styles/story_page/public/story/red-roofs-are-haunting-pizza-huts-sales.jpg?itok=z0D4czud" alt="Pizza photo">

      <mat-card-title>{{card.title}}</mat-card-title>
      <mat-card-subtitle>Calorie : {{card.calories}}</mat-card-subtitle>
      <mat-card-subtitle>ID : {{card.id}}</mat-card-subtitle>
      <mat-card-subtitle>Date : {{card.date}}</mat-card-subtitle>
      <mat-card-subtitle>Time : {{card.time}}</mat-card-subtitle>
      <mat-card-subtitle>Day Intake : {{card.dailyCalorieIntake}}</mat-card-subtitle>
      <div class="card-box-red" *ngIf="card.dailyCalorieIntake>=card.dailyCalorieGoal"><div class="card-circle-red"></div></div>
      <div class="card-box-green" *ngIf="card.dailyCalorieIntake<card.dailyCalorieGoal"><div class="card-circle-green"></div></div>
    </mat-card>
</div>
    </div>
    <div class="pagination-container">
      <button mat-raised-button color="primary" *ngIf="canNavigateBack()" (click)="applyLimitAndOffset(-1)"> <- </button>
      <button mat-raised-button color="primary" *ngIf="canNavigateForward()" (click)="applyLimitAndOffset(1)"> -> </button>
    </div>

  `,
  styles: [`
  .mealCard{
    display: inline-block;
    max-width: 15rem;
    margin: 1rem;
    cursor: pointer;
  }
    .card-box-red, .card-box-green{
      border: 2px solid red;
      padding: 0.2rem;
      display: inline-block;
      background-color:white
    }
    .card-box-green{
      border: 2px solid green;
    }
    .card-circle-red, .card-circle-green{
      background-color: red;
      width:6px;
      height:6px;
      border-radius : 50%;
    }
    .card-circle-green{
      background-color: green;
    }
    .filterBox{
      display: inline-block;
    }
    .parentContainer{
      display: flex;
    }
    .pagination-container{
      text-align: center;
    }
    .pagination-container button{
      margin: 0 0.5rem;
    }
  `]
})
export class MealListContainer implements OnInit  {
  @Select(LoginState) data$: Observable<LoginModel>;

  mealsArray = [];
  tempMealArray: IMeal[] = [];
  filteredArray: IMeal[];
  calorieMin: string;
  calorieMax: string;
  timeMax: string;
  timeMin: string;
  dateMax: string;
  dateMin: string;
  dayCalorie: number;
  calorieGoal: number;
// limit should be 50
  limit = 6;
  offset = 0;
  constructor(private mealService: MealService, private router: Router, private toastr: ToastrService) {
    this.dayCalorie = 800;
    this.calorieGoal = 600;
  }

  ngOnInit() {
    const searchParams = {
      limit: this.limit,
      offset: this.offset,
      calorieMin: this.calorieMin,
      calorieMax: this.calorieMax,
      timeMin: this.timeMin,
      timeMax: this.timeMax,
      dateMin: this.dateMin,
      dateMax: this.dateMax
    };
    this.mealService.fetchMeals(searchParams).then((res: IResponse) => {
     this.mealsArray = res.data;
     this.tempMealArray = this.mealsArray;
     this.applyLimitAndOffset();
   }).catch((e) => {
     this.toastr.error(e);
    });
  }

  applyFilters() {
    let response: {success: boolean, title?: string, message?: string};
    response = ValidationService.isValidFilter({maxTime: this.timeMax, minTime: this.timeMin,
                maxDate: this.dateMax, minDate: this.dateMin});
    if (!response.success) {
      this.toastr.error(response.message, response.title);
    } else {
      this.offset = 0;
      this.fetchMealsFromService();

    }

  }

  fetchMealsFromService() {
    const searchParams = {
      limit: this.limit || undefined,
      offset: this.offset || undefined,
      calorieMin: this.calorieMin || undefined,
      calorieMax: this.calorieMax || undefined,
      timeMin: this.timeMin || undefined,
      timeMax: this.timeMax || undefined,
      dateMin: this.dateMin || undefined,
      dateMax: this.dateMax || undefined,
    };
    this.mealService.fetchMeals(searchParams).then((res: IResponse) => {
      this.mealsArray = res.data;
      this.tempMealArray = this.mealsArray;
      this.applyLimitAndOffset();
    }).catch((e) => {
      this.toastr.error(e);
    });
  }
  applyLimitAndOffset(index?) {
    this.tempMealArray = this.mealsArray;
    if (index === 1 ) {
      this.offset += this.limit;
      this.fetchMealsFromService();
    }
    if (index === -1 ) {
      if (this.offset - 6 >= 0 ) {
        this.offset -= this.limit;
        this.fetchMealsFromService();
      }
    }
  }

  canNavigateBack() {
    return this.offset > 0;
  }
  canNavigateForward() {
    return this.tempMealArray.length === this.limit;
  }

  updateMeal(id) {
    this.router.navigateByUrl(`meal-update/${id}`);
  }

}
