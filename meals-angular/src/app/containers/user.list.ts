import {Component, OnInit} from '@angular/core';
import {Select} from '@ngxs/store';
import {LoginState} from '../state/state';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {IUser} from '../../interfaces/IUser';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import {IResponse} from '../../interfaces/IResponse';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  template: `
    <div class="userList-container">
      <div class="userListCards">
        <mat-card class="userListCard" *ngFor="let user of userArray" (click)="userCardClickHandler(user.id)">
          <div class="matCard-image">
            <img mat-card-image
                 src="https://microhealth.com/assets/images/illustrations/personal-user-illustration-@2x.png">
          </div>
          <div class="matCard-text">
            <mat-card-title>{{user.name}}</mat-card-title>
            <mat-card-subtitle>Email : {{user.email}}</mat-card-subtitle>
            <mat-card-subtitle>CalorieGoal : {{user.calorieGoal}}</mat-card-subtitle>
            <mat-card-subtitle>ID : {{user.id}}</mat-card-subtitle>
            <mat-card-subtitle>Role : {{user.role}}</mat-card-subtitle>
            <mat-card-subtitle>Gender : {{user.sex}}</mat-card-subtitle>
            <mat-card-subtitle>Calorie Goal : {{user.calorieGoal}}</mat-card-subtitle>
          </div>
        </mat-card>
      </div>
    </div>
  `,
  styles: [`
    .userList-container {
      text-align: center;

      background-color: #e6e6e6;
    }

    .userListCards {
      padding: 4rem 0;
    }

    .userListCard {
      display: flex;
      justify-content: center;
      cursor: pointer;
      width: 40%;
      margin: 1rem auto;
    }

    .matCard-image {
      width: 30%;
      display: inline-block;
      padding-top: 1rem;
    }

    .userListCard img {
      max-width: 8rem;
    }
  `]
})
export class UserListContainer implements OnInit {
  @Select(LoginState) data$: Observable<LoginModel>;
  userArray: IUser[] = [];

  constructor(private toastr: ToastrService, private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.userService.getUsers().then((u: IResponse) => {
      this.userArray = u.data;
    }).catch((e) => {
      this.toastr.error(e);
    });
  }

  userCardClickHandler(id: any) {
    this.router.navigateByUrl(`user-update/${id}`);
  }
}
