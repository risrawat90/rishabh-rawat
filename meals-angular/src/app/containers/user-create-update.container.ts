import {Component, OnDestroy, OnInit} from '@angular/core';
import {Select} from '@ngxs/store';
import {LoginState} from '../state/state';
import {Observable, Subscription} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {IUser} from '../../interfaces/IUser';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {IResponse} from '../../interfaces/IResponse';
import ValidationService from '../services/validation.service';
import {ERole} from '../../enums/ERole';
import {EGender} from '../../enums/EGender';

// /
@Component({
  selector: 'app-user-create-update',
  template: `
    <div class="user-create-form">
      <div class="user-create-heading">
        <span>{{isCreateUser ? 'User Creation' : 'User Updation' }} </span>
      </div>
      <div class="user-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Name</mat-label>
          <input [(ngModel)]="user.name" matInput placeholder="Aaron">
        </mat-form-field>
      </div>


      <div class="user-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Email</mat-label>
          <input [(ngModel)]="user.email" matInput placeholder="frankcastle@gmail.com">
        </mat-form-field>
      </div>

      <div class="user-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Password</mat-label>
          <input [(ngModel)]="user.password" matInput type="password" placeholder="*****">
        </mat-form-field>
      </div>

      <div class="user-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Calorie Goal</mat-label>
          <input [(ngModel)]="user.calorieGoal" matInput placeholder="2000">
        </mat-form-field>
      </div>

      <div class="user-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Role</mat-label>
          <mat-select [disabled]="currentUser.role !== 'admin' " [(ngModel)]="user.role">
            <mat-option *ngFor="let roleVal of roleArray" [value]="roleVal">
              {{roleVal}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>

      <div class="user-create-input">
        <mat-form-field appearance="outline">
          <mat-label>Gender</mat-label>
          <mat-select [(ngModel)]="user.sex">
            <mat-option *ngFor="let genderVal of genderArray" [value]="genderVal">
              {{genderVal}}
            </mat-option>
          </mat-select>
        </mat-form-field>
        <div *ngIf="isCreateUser">
          <button (click)="createUser()" mat-raised-button color="primary">Create User</button>
        </div>
        <div *ngIf="!isCreateUser">
          <button (click)="updateUser()" mat-raised-button color="primary">Update User</button>
          <button class="deleteUserBtn" (click)="deleteUser()" mat-raised-button color="primary">Delete User</button>
        </div>

      </div>
    </div>
  `,
  styles: [`
    .user-card {
      max-width: 400px;
    }

    .user-card img {
      max-width: 8rem;
    }

    .user-card {
      text-align: center;
      margin: 0 auto;
    }

    .user-create-form {
      padding-top: 3rem;
      width: 50%;
      text-align: center;
      margin: 0 auto;
    }

    .user-create-heading span {
      font-size: 3rem;
      font-family: "BryantProRegular", 'Helvetica Neue', helvetica, arial, sans-serif, "Helvetica Neue";
    }

    .user-create-input mat-form-field {
      width: 70%;
    }

    .deleteUserBtn {
      margin: 1rem;
    }
  `]
})
export class UserCreateUpdateContainer implements OnInit, OnDestroy {
  @Select(LoginState) data$: Observable<LoginModel>;

  id = null;
  isCreateUser = true;
  user: IUser = {
    name: '',
    role: ERole.CONSUMER,
    sex: EGender.MALE,
    calorieGoal: 100,
    email: '',
    password: ''
  };
  currentUser: IUser;

  subscription: Subscription;

  roleArray = Object.values(ERole)
  genderArray = ['male', 'female', 'other'];

  constructor(private toastr: ToastrService, private userService: UserService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.isCreateUser = false;

      this.userService.getUserById(this.id).then((u: IResponse) => {
        this.user = u.data;
      }).catch((e) => {
        this.toastr.error(e);
      });
    }
    this.subscription = this.data$.subscribe(u => {
      this.currentUser = u.user;
    });

  }

  canCreateUser() {
    const x: boolean = this.currentUser && (this.currentUser.role === 'admin' || this.currentUser.role === 'manager');
    return x;
  }

  async createUser() {
    const userObj = {
      name: this.user.name,
      password: this.user.password,
      role: this.user.role,
      email: this.user.email,
      sex: this.user.sex,
      calorieGoal: this.user.calorieGoal,
    };

    let response: { success: boolean, message?: string };
    response = ValidationService.isValidCreateUser({...userObj});
    console.log(response);
    if (!response.success) {
      this.toastr.error(response.message);
    } else {
      this.userService.createUser(userObj).then((res) => {
        this.toastr.success('User Created Successfully.');
        this.router.navigateByUrl('user-list');
      }).catch((e) => {
        this.toastr.error(e);
      });
    }

  }

  async updateUser() {
    const userObj = {
      name: this.user.name,
      password: this.user.password,
      role: this.user.role,
      email: this.user.email,
      sex: this.user.sex,
      calorieGoal: this.user.calorieGoal,
    };
    let response: { success: boolean, message?: string };
    response = ValidationService.isValidUpdateUser({...userObj});
    console.log(response);
    if (!response.success) {
      this.toastr.error(response.message);
    } else {
      this.userService.updateUserById(this.user).then(() => {
        this.toastr.success('User Updated Successfully');
        this.router.navigateByUrl(`user-update/${this.user.id}`);
      }).catch((e) => {
        this.toastr.error(e);
      });
    }
  }

  async deleteUser() {
    this.userService.deleteUserById(this.id).then((res) => {
      this.toastr.success('Record Deleted Successfully');
      this.router.navigateByUrl('user-list');
    }).catch((e) => {
      this.toastr.error(e);
    }).catch((e) => {
      this.toastr.error(e);
    });
  }

  ngOnDestroy(): void {
    if (this.subscription){
      this.subscription.unsubscribe();
    }
  }

}
