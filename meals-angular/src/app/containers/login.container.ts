import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';
import {LoginState} from '../state/state';
import {ToastrService} from 'ngx-toastr';
import ValidationService from '../services/validation.service';

@Component({
  selector: 'app-login',
  template: `
    <div class="login-container">
      <div class="login-input">
      <mat-form-field appearance="outline">
        <mat-label>Email</mat-label>
        <input [(ngModel)]="email" matInput placeholder="Email">
      </mat-form-field>
      </div>
      <div class="login-input">
      <mat-form-field appearance="outline">
        <mat-label>Password</mat-label>
        <input [(ngModel)]="password" type="password" matInput placeholder="Password">
      </mat-form-field>
      </div>
      <button (click)="handleLogin()" mat-raised-button color="primary">Login</button>

    </div>
  `,
  styles: [`
    .login-container {
      text-align: center;
    }
    .login-input mat-form-field{
      width: 40%;
    }
  `]
})
export class LoginContainer implements OnInit {

  @Select(LoginState) data$: Observable<LoginModel>;
  public email = 'gohan123@gmail.com';
  public password = 'gohan';
  loggedIn = false;
  constructor(private readonly router: Router,
              private readonly userService: UserService,
              private store: Store,
              private toastr: ToastrService,
              ) {
  }

  ngOnInit() {
    this.data$.subscribe(u => {
      this.loggedIn = u.isLoggedIn;
    });

  }

  public async handleLogin() {

    let response: {success: boolean, message?: string};
    response = ValidationService.isValidLogin({email: this.email, password: this.password});
    console.log(response);
    if (!response.success) {
      this.toastr.error(response.message);
    } else {

      try {
        const res = await this.userService.login({email: this.email, password: this.password});
        this.toastr.success('Login Successful');
        this.router.navigateByUrl('home');
      } catch (e)  {
        this.toastr.error(e);
      }

    }
  }

}
