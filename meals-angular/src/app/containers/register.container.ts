import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import ValidationService from '../services/validation.service';
import {ToastrService} from 'ngx-toastr';
import {ERole} from "../../enums/ERole";

// email password name role gender confirmPassword
@Component({
  selector: 'app-register',
  template: `
    <div class="register-form">
      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Name</mat-label>
          <input [(ngModel)]="name" matInput placeholder="James Cooper">
        </mat-form-field>
      </div>
      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Email</mat-label>
          <input [(ngModel)]="email" matInput placeholder="Email">
        </mat-form-field>
      </div>
      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Password</mat-label>
          <input [(ngModel)]="password" type="password" matInput placeholder="Password">
        </mat-form-field>
      </div>
      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Confirm Password</mat-label>
          <input [(ngModel)]="Cpassword" type="password" matInput placeholder="Confirm Password">
        </mat-form-field>
      </div>
      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Calorie Goal</mat-label>
          <input [(ngModel)]="calorieGoal" type="text" matInput placeholder="1700">
        </mat-form-field>
      </div>
      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Role</mat-label>
          <mat-select [(ngModel)]="role">
            <mat-option *ngFor="let roleVal of roleArray" [value]="roleVal">
              {{roleVal}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>

      <div class="register-input">
        <mat-form-field appearance="outline">
          <mat-label>Gender</mat-label>
          <mat-select [(ngModel)]="gender">
            <mat-option *ngFor="let genderVal of genderArray" [value]="genderVal">
              {{genderVal}}
            </mat-option>
          </mat-select>
        </mat-form-field>
        <div>

          <button (click)="handleRegister()" mat-raised-button color="primary">Register</button>

        </div>
  `,
  styles: [`
    .register-form {
      text-align: center;
    }

    .register-input mat-form-field {
      width: 40%;
    }
  `]
})
export class RegisterContainer implements OnInit {

  roleArray = Object.values(ERole)
  genderArray = ['male', 'female', 'other'];
  name = 'beerus sama';
  role = 'admin';
  gender = 'male';
  email = 'user1.regular@gmail.com';
  password = 'abcd1234';
  Cpassword = 'abcd1234';
  calorieGoal = 1245;
  constructor(private userService: UserService, private router: Router, private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  handleRegister() {
    console.log(this.name, this.email, this.password, this.Cpassword , this.role, this.gender);
    const personObject = {
      name: this.name,
      password: this.password,
      email: this.email,
      role: this.role,
      sex: this.gender,
      calorieGoal: this.calorieGoal,
    };
    let response: {success: boolean, message?: string};
    response = ValidationService.isValidUserData({...personObject, Cpassword: this.Cpassword});
    console.log(response);
    if (!response.success) {
        this.toastr.error(response.message);
    } else {
      this.userService.register(personObject).then(() => {
        this.router.navigateByUrl('login');
      }).catch((e) => {
        this.toastr.error(e);
      });
    }

  }


}
