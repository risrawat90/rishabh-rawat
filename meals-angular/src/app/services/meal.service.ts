import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Select} from '@ngxs/store';
import {LoginState} from '../state/state';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';

@Injectable({
  providedIn: 'root'
})
export class MealService {
  @Select(LoginState) data$: Observable<LoginModel>;

  ERROR_STRING = 'Some Error Occured';
  constructor(private http: HttpClient, private router: Router) {
  }


  url = 'http://localhost:3000/meal';

    static getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
  }

  getHeader() {
    return new HttpHeaders({
      jwt: localStorage.getItem('jwttoken'),
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
  }

  async fetchMeals({limit, offset= 0, calorieMin= '0', calorieMax= '2000', timeMin= '00:00'  , timeMax= '23:59', dateMin= '01-01-1995', dateMax= '30-12-3000'}) {
      const searchParams = {limit, offset, calories: [calorieMin, calorieMax], time: [timeMin, timeMax], date: [dateMin, dateMax]};
      const queryString = `?limit=${limit}&offset=${offset}&calorieMin=${calorieMin}&calorieMax=${calorieMax}&timeMin=${timeMin}&timeMax=${timeMax}&dateMin=${dateMin}&dateMax=${dateMax}`;

      return new Promise((resolve, reject) => {
      this.http.get(this.url + queryString, {headers: this.getHeader()})
        .subscribe((res) => {
          resolve(res);
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error.message) {
              const errorMessage = err.error.message;
              reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }

  async getMealById(payload) {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/' + payload, {headers: this.getHeader()}).subscribe((res) => {
        resolve(res);
      }, (err) => {
        if (err.status === 401) {
          this.router.navigateByUrl('login');
        } else {
          if (err.error.message) {
            const errorMessage = err.error.message;
            reject(errorMessage);
          }
          reject(this.ERROR_STRING);
        }
      });
    });
  }

  async updateMealById(payload) {
    return new Promise((resolve, reject) => {
      this.http.put(this.url + `/${payload.id}`, payload, {headers: this.getHeader()})
        .subscribe((res) => {
          resolve(res);
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error.message) {
              const errorMessage = err.error.message;
              reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }

  async createMeal(payload) {
      return new Promise((resolve, reject) => {
      this.http.post(this.url + `/`, payload, {headers: this.getHeader()})
        .subscribe((res) => {
          resolve(res);
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error.message) {
              const errorMessage = err.error.message;
              reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }

  async deleteMealById(payload) {
      return new Promise((resolve, reject) => {
        this.http.delete(this.url + `/${payload}`, {headers: this.getHeader()})
          .subscribe((res) => {
            resolve(res);
          }, (err) => {
            if (err.status === 401) {
              this.router.navigateByUrl('login');
            } else {
              if (err.error.message) {
                const errorMessage = err.error.message;
                reject(errorMessage);
              }
              reject(this.ERROR_STRING);
            }
          });
      });
  }

}
