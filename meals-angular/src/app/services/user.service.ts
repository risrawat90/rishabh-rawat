import {Injectable} from '@angular/core';
// @ts-ignore
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Select, Store} from '@ngxs/store';
import {LoginAction, LoginActionOnRefresh, LogoutAction} from '../state/state.actions';
import {IResponse} from '../../interfaces/IResponse';
import {Router} from '@angular/router';
import {LoginState} from '../state/state';
import {Observable} from 'rxjs';
import {LoginModel} from '../state/login.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'http://localhost:3000/user';
  ERROR_STRING = 'Some Error Occured';
  @Select(LoginState) data$: Observable<LoginModel>;


  constructor(private http: HttpClient, private store: Store, private router: Router) {
  }

  getHeader() {
    return new HttpHeaders({
      jwt: localStorage.getItem('jwttoken') || '',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
  }

  async getUsers() {
    return new Promise((resolve, reject) => {
      this.http.get(this.url , {headers: this.getHeader()}).subscribe(res => {
        console.log(res);
        resolve(res);
      }, (err) => {
        if (err.status === 401) {
          this.router.navigateByUrl('login');
        } else {
          if (err.error.message) {
            const errorMessage = err.error.message;
            reject(errorMessage);
          }
          reject(this.ERROR_STRING);
        }
      });
    });
  }

  async getLoggedInUserIfAny() {
    console.log('Get logged in user /details triggered');
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/details', {headers: this.getHeader()})
        .subscribe((res: IResponse) => {
          console.log(res.data);
          this.store.dispatch(new LoginActionOnRefresh(res.data));
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  async createUser(payload) {
    console.log(payload);
    return new Promise((resolve, reject) => {
      this.http.post(this.url, payload, {headers: this.getHeader()}).subscribe(res => {
        console.log(res);
        resolve(res);
      }, (err) => {
        if (err.status === 401) {
          this.router.navigateByUrl('login');
        } else {
          if (err.error.message) {
            const errorMessage = err.error.message;
            reject(errorMessage);
          }
          reject(this.ERROR_STRING);
        }
      });
    });
  }

  async login(payload) {
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/login', payload, {})
        .subscribe( (res: IResponse) => {
          console.log(res);
          this.store.dispatch(new LoginAction(res.data)).subscribe(() => resolve());
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error) {
              const errorMessage = err.error.message;
              reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }

  async updateUserById(payload) {
    return new Promise((resolve, reject) => {
      this.http.put(this.url + `/${payload.id}`, payload, {headers: this.getHeader()})
        .subscribe((res) => {
          console.log(res);
          resolve();
        }, (err) => {
        if (err.status === 401) {
          this.router.navigateByUrl('login');
        } else {
          if (err.error.message) {
            const errorMessage = err.error.message;
            reject(errorMessage);
          }
          reject(this.ERROR_STRING);
        }
      });
    });
  }

  async logout() {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/logout',  {headers: this.getHeader()})
        .subscribe( (res: IResponse) => {
          console.log(res);
          this.store.dispatch(new LogoutAction());
          resolve();
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error.message) {
            const errorMessage = err.error.message;
            reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
    }

  async register(payload) {
    console.log(payload);
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/register', payload, {headers: this.getHeader()})
        .subscribe((res) => {
          resolve();
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            console.log(err);
            if (err) {
              reject(err.error.message);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }

  async getUserById(payload) {
    console.log(payload);
    return new Promise((resolve, reject) => {
      this.http.get(this.url + `/${payload}`, {headers: this.getHeader()})
        .subscribe((res) => {
          console.log(res);
          resolve(res);
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error.message) {
              const errorMessage = err.error.message;
              reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }

  async deleteUserById(payload) {
    return new Promise((resolve, reject) => {
      this.http.delete(this.url + `/${payload}`, {headers: this.getHeader()})
        .subscribe((res) => {
          console.log(res);
          resolve('Record Deleted Successfully');
        }, (err) => {
          if (err.status === 401) {
            this.router.navigateByUrl('login');
          } else {
            if (err.error.message) {
              const errorMessage = err.error.message;
              reject(errorMessage);
            }
            reject(this.ERROR_STRING);
          }
        });
    });
  }


}
