import {ERole} from '../../enums/ERole';

const roleArray = Object.values(ERole);
const genderArray = ['male', 'female', 'other'];
export default class ValidationService {


  constructor() {}

  static isValidUserData({name, email, password, Cpassword, role, sex, calorieGoal}) {

    if (!name || name.trim().length < 3) {
      return {success: false, message: 'Name is too short!'};
    }

    if (!email || !this.isValidEmail(email)) {
      return {success: false, message: 'Invalid Email'};
    }

    if (!password || password !== Cpassword) {
      return {success: false, message: 'Passwords do not match'};
    }

    if (password.length < 8) {
      return {success: false, message: 'Password too short, Min Length: 8 chars'};
    }

    if (!role || !roleArray.includes(role)) {
      return {success: false, message: 'Role Required'};
    }

    if (!sex || !genderArray.includes(sex)) {
      return {success: false, message: 'Gender Required'};
    }

    if (!calorieGoal) {
      return {success: false, message: 'Calorie Goal Required'};
    }

    return {success: true};
  }

  static isValidLogin({email, password}) {
    if (!email) {
      return {success: false, message: 'Email Required'};
    }
    if (!password) {
      return {success: false, message: 'Password Required'};
    }
    return {success: true};
  }
  static isValidCreateUser({ name, role, email, sex, password, calorieGoal}) {

    if (!name || name.trim().length < 3) {
      return {success: false, message: 'Name is too short!'};
    }

    if (!email || !this.isValidEmail(email)) {
      return {success: false, message: 'Invalid Email'};
    }

    if (!password || password.length < 3) {
      return {success: false, message: 'Password too short, Min Length: 3 chars'};
    }

    if (!calorieGoal ) {
      return {success: false, message: 'Calorie Goal Required'};
    }

    if (!role || !roleArray.includes(role)) {
      return {success: false, message: 'Role Required'};
    }

    if (!sex || !genderArray.includes(sex)) {
      return {success: false, message: 'Gender Required'};
    }

    return {success: true};

  }

  static isValidUpdateUser({ name, role, email, sex, password, calorieGoal}) {
    if (!password) { password = 'dummy'; }
    return this.isValidCreateUser({name, role, email, sex, password, calorieGoal});
  }

  static isValidMeal({time, calories, date, title, id}) {
    if (!title || title.length < 10 || title.length > 50) {
      return {success: false, message: 'Meal Name should be in range 10 to 50 words'};
    }
    if (!calories) {
      return {success: false, message: 'Calorie Required'};
    }
    if ( !date || !this.isValidDate(date)) {
      return {success: false, title: 'Date Invalid', message: 'Format: YYYY-DD-MM'};
    }
    if ( !time || !this.isValidTime(time)) {
      return {success: false, title: 'Time Invalid', message: 'Format: HH:MM'};
    }
    return {success: true};
  }
  static isValidFilter({maxTime, minTime, maxDate, minDate}) {
    if (maxTime) {
      if (!this.isValidTime(maxTime)) {
        return {success: false, title: 'Time Invalid', message: 'Format: HH:MM'};
      }
    }
    if (minTime) {
      if (!this.isValidTime(minTime)) {
        return {success: false, title: 'Time Invalid', message: 'Format: HH:MM'};
      }
    }
    if (maxDate) {
      if (!this.isValidDate(maxDate)) {
        return {success: false, title: 'Date Invalid', message: 'Format: YYYY-DD-MM'};
      }
    }

    if (minDate) {
      if (!this.isValidDate(minDate)) {
        return {success: false, title: 'Date Invalid', message: 'Format: YYYY-DD-MM'};
      }
    }
    return {success: true};
  }

   static isValidDate(dateString) {
     // const mystring = dateString.split('-').join('/');
     const pattern = /^\d{4}-\d{2}-\d{2}$/;
     return pattern.test(dateString);
     }

     static isValidTime(timeString) {
       return /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/.test(timeString);

     }
  static isValidUrl(str: string) {
    const regex = '^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&\'\\(\\)\\*\\+,;=.]+$';
    return str.match(regex);
  }
  static isValidEmail(str: string) {
    const regex = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@'
      + '[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
    return str.match(regex);
  }

}
