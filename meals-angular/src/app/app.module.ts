import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {NgxsModule} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginContainer} from './containers/login.container';
import {RegisterContainer} from './containers/register.container';
import {HomeContainer} from './containers/home.container';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {ToastrModule} from 'ngx-toastr';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {UserService} from './services/user.service';
import {LoginState} from './state/state';
import {MealCreateContainer} from './containers/meal.create';
import {MealListContainer} from './containers/meals.list';
import {UserListContainer} from './containers/user.list';
import {NavbarContainer} from './containers/navbar.container';
import {AuthGuard} from './guard/auth.guard';
import {MealService} from './services/meal.service';
import {HttpClientModule} from "@angular/common/http";
import {UserCreateUpdateContainer} from "./containers/user-create-update.container";

@NgModule({
  declarations: [
    AppComponent,
    LoginContainer,
    RegisterContainer,
    HomeContainer,
    MealCreateContainer,
    MealListContainer,
    UserListContainer,
    NavbarContainer,
    UserCreateUpdateContainer
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    NgxsModule.forRoot([LoginState], {developmentMode: true}),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),

    HttpClientModule,

    ToastrModule.forRoot(),
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatToolbarModule,
    MatDatepickerModule
  ],
  providers: [UserService, AuthGuard, MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }
