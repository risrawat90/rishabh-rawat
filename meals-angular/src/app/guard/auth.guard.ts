import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Select} from '@ngxs/store';
import {LoginState} from '../state/state';
import {LoginModel} from '../state/login.model';
import {UserService} from '../services/user.service';


@Injectable()
export class AuthGuard implements CanActivate {

  @Select(LoginState) data$: Observable<LoginModel>;
  loggedIn: boolean ;
  constructor(private router: Router, private userService: UserService) {
    this.data$.subscribe(async u => {
      this.loggedIn = u.isLoggedIn;
    });
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean
    | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    console.log('Auth Guard Triggered');

    if (!this.loggedIn) {
      console.log('IF');
      this.userService.getLoggedInUserIfAny().then(() => {
            if (!this.loggedIn) {
              console.log('NESTED IF');
              this.router.navigateByUrl('login');
            } else {   console.log('Return true');
                       return true;
            }
          }).catch((e) => {
            this.router.navigateByUrl('login');
      });
    }
    return true;
  }
}
