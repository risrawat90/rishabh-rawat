import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginContainer} from './containers/login.container';
import {RegisterContainer} from './containers/register.container';
import {HomeContainer} from './containers/home.container';
import {MealCreateContainer} from './containers/meal.create';
import {MealListContainer} from './containers/meals.list';
import {UserListContainer} from './containers/user.list';
import {AuthGuard} from './guard/auth.guard';
import {UserCreateUpdateContainer} from './containers/user-create-update.container';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path: 'login', component: LoginContainer},
  {path: 'register', component: RegisterContainer},
  {path: 'home', component: HomeContainer, canActivate: [AuthGuard]},
  {path: 'user-update/:id', component: UserCreateUpdateContainer, canActivate: [AuthGuard]},
  {path: 'user-create', component: UserCreateUpdateContainer, canActivate: [AuthGuard]},
  {path: 'user-list', component: UserListContainer, canActivate: [AuthGuard]},
  {path: 'meal-create', component: MealCreateContainer, canActivate: [AuthGuard]},
  {path: 'meal-update/:id', component: MealCreateContainer, canActivate: [AuthGuard]},
  {path: 'meal-list', component: MealListContainer, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
